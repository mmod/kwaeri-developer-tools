/**
 * SPDX-PackageName: kwaeri/developer-tools
 * SPDX-PackageVersion: 0.10.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES
import * as assert from 'assert';
import { kdt } from "../src/kdt.mjs"


// GLOBALS
const _ = new kdt();

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'Kwaeri Developer Tools Functionality Test Suite',
    () => {

        describe(
            'Check Type Test [I - boolean]',
            () => {
                it(
                    'Should return true when checking a boolean type value.',
                    () => {
                        assert.equal(
                            _.type( true ),
                            'boolean'
                        );
                    }
                );
            }
        );


        describe(
            'Check Type Test [II - number]',
            () => {
                it(
                    'Should return true when checking a number type value.',
                    () => {
                        assert.equal(
                            _.type( 1 ),
                            'number'
                        );
                    }
                );
            }
        );


        describe(
            'Check Type Test [III - number]',
            () => {
                it(
                    'Should return true when checking a string type value.',
                    () => {
                        assert.equal(
                            _.type( "abode" ),
                            'string'
                        );
                    }
                );
            }
        );


        describe(
            'Check Type Test [IV - function]',
            () => {
                it(
                    'Should return true when checking a function type value.',
                    () => {
                        assert.equal(
                            _.type( ()=>{} ),
                            'function'
                        );
                    }
                );
            }
        );


        describe(
            'Check Type Test [V - array]',
            () => {
                it(
                    'Should return true when checking an array type value.',
                    () => {
                        assert.equal(
                            _.type( [] ),
                            'array'
                        );
                    }
                );
            }
        );


        describe(
            'Check Type Test [VI - date]',
            () => {
                it(
                    'Should return true when checking a date type value.',
                    () => {
                        assert.equal(
                            _.type( new Date() ),
                            'date'
                        );
                    }
                );
            }
        );


        describe(
            'Check Type Test [VII - regexp]',
            () => {
                it(
                    'Should return true when checking a regexp type value.',
                    () => {
                        assert.equal(
                            _.type( /regexp/mg ),
                            'regexp'
                        );
                    }
                );
            }
        );


        describe(
            'Check Type Test [VIII - object]',
            () => {
                it(
                    'Should return true when checking an object type value.',
                    () => {
                        assert.equal(
                            _.type( {} ),
                            'object'
                        );
                    }
                );
            }
        );


        describe(
            'Check Type Test [IX - comparing mismatched types]',
            () => {
                it(
                    'Should return true when all tests fail, indicating type checking is accurate (to a degree).',
                    () => {
                        // Create the types, have each be wrong with no pattern otherwise:
                        let booleanType         = function(){},
                            numberType          = 'abcde',
                            stringType          = 1,
                            functionType: any[] = [],
                            arrayType           = new Date(),
                            regexpType          = true,
                            objectType          = /regexp/mg,
                            dateType            = {};

                        // Check if any test comes back true
                        const result = (
                            ( _.type( booleanType )   === 'boolean' )   ||
                            ( _.type( numberType )    === 'number' )    ||
                            ( _.type( stringType )    === 'string' )    ||
                            ( _.type( functionType )  === 'function' )  ||
                            ( _.type( arrayType )     === 'array' )     ||
                            ( _.type( regexpType )    === 'regexp' )    ||
                            ( _.type( objectType )    === 'object' )    ||
                            ( _.type( dateType )      === 'date' )

                        ) ? true : false;

                        // They shouldn't
                        assert.equal(
                            result,
                            false
                        );
                    }
                );
            }
        );


        describe(
            'Check Empty Test [I - Empty]',
            () => {
                it(
                    'Should return true when asserting an empty object is empty.',
                    () => {
                        assert.equal(
                            _.empty( {} ),
                            true
                        );
                    }
                );
            }
        );


        describe(
            'Check Empty Test [II - Non-empty object]',
            () => {
                it(
                    'Should return true when asserting a non-empty object is not empty.',
                    () => {
                        assert.equal(
                            _.empty( { "name": "tester" } ),
                            false
                        );
                    }
                );
            }
        );


        describe(
            'Extend Object Test [I - As expected]',
            () => {
                it(
                    'Should return true when extending an object as expected.',
                    () => {
                        const objectA = {
                            "first": "hello",
                            "second": "world",
                            "third": { "first": "hello", "second": { "first": "goodbye" }, "third": { "first": "hello", "second": "world", "third": "!", "fourth": "Ok" } },
                            "fourth": "hahaha",
                            "fifth": { "first": "hello", "second": "world" },
                            "sixth": "Please."
                        };
                        const objectB = {
                            "first": "hello",
                            "second": "world",
                            "third": { "first": "goodbye", "second": { "first": "hello", "second": "world", "third": "!" }, "third": { "first": "goodbye", "second": "world", "third": "?" } },
                            "fourth": "hehehe",
                            "fifth": "Of course!",
                            "seventh": "Thank you!"
                        };

                        // Proper tests always include the expected result as a reference to compare with
                        const expectedObject = {
                            // Both have a property named first with the value
                            // hello, so we kept the first's
                            "first": "hello",
                            // The same with the second as with the first
                            "second": "world",
                            // In third, both have an object with several
                            // properties. So these objects are compared and
                            // extended.
                            //   * In both, a property named first exists and
                            //   and while both have a different value, we
                            //   keep the first's because it already existed
                            //   and we honor that.
                            //   * In both, a property exists  named second
                            //   that is an object, so again these objects
                            //   are compared and the first's extended by the
                            //   second's
                            //     - in checking the objects, we keep the
                            //     properties in the first that already exist,
                            //     and extend that object with any properties
                            //     that do not.
                            //       * Hence, first remains "goodbye", rather
                            //       than changes to "hello"
                            //       * However, second and third do not exist,
                            //       so they are appended to the first object.
                            // In third, both also have a property named third
                            // that is also an object.
                            //   * In checking that object, we again keep
                            //   properties in the first that already exist,
                            //   and extend that object with any properties
                            //   that do not
                            //     * Hence, "fourth": "Ok" gets kept in the
                            //     object in the first objects chain, and the
                            //     remaining differences in second objects
                            //     chain are not reflected in the first due to
                            //    the fact that they can't overwrite them.
                            "third": { "first": "hello", "second": { "first": "goodbye", "second": "world", "third": "!" }, "third": { "first": "hello", "second": "world", "third": "!", "fourth": "Ok" } },
                            // fourth is different in the first object, but
                            // because it already exists we keep it, and do
                            // not change it to the second objects value.
                            "fourth": "hahaha",
                            // Fifth is an object in the first object, but not
                            // in the second. However, its not for that reason
                            // that we keep the object in the first, but rather
                            // because fifth already existed that we kept it as
                            // it was.
                            "fifth": { "first": "hello", "second": "world" },
                            // Sixth does exist in the first object, so we
                            // kept it even though it didn't exist in the
                            // second object.
                            "sixth": "Please.",
                            // seventh does NOT exist in the first object,
                            // so we appended it because it did exist in
                            // the second object.
                            "seventh": "Thank you!"

                        };

                        // And deliver the actual results to compare with
                        const resultingObject = _.extend( objectA, objectB );;

                        assert.equal(
                            ( JSON.stringify( expectedObject ) === JSON.stringify( resultingObject ) ),
                            true
                        );
                    }
                );
            }
        );


        describe(
            'Each Test [I - Array]',
            () => {
                it(
                    'Should return true when each array indice has its first string character uppercas-ed.',
                    () => {
                        // A list of some type to use in our test
                        const fruits = ["apple", "orange", "banana", "plum", "grape" ];

                        // Now we are going to loop throught the list of fruits and make another list of fruit-salad types
                        let saladTypes: string[] = [];

                        const processed = _.each(
                            fruits,
                            ( index: any, fruit: any ) => {
                                ( saladTypes as any )[index] = fruit.charAt(0).toUpperCase() + fruit.slice(1) + ' Salad';
                            }
                        );

                        // We need a control to compare against in our test:
                        const expectedSaladTypes = ['Apple Salad', 'Orange Salad', 'Banana Salad', 'Plum Salad', 'Grape Salad'];

                        assert.equal(
                            ( processed && ( JSON.stringify( saladTypes ) === JSON.stringify( expectedSaladTypes ) ) ),
                            true
                        );
                    }
                );
            }
        );


        describe(
            'Has Test [I - Nested object property denoted with a string]',
            () => {
                it(
                    'Should assert true when the object property exists, value or not.',
                    () => {
                        assert.equal(
                            _.has(
                                { deeply: { nested: { testProp: "An existing value." } } },
                                'deeply.nested.testProp'
                            ),
                            true
                        );
                    }
                );
            }
        );


        describe(
            'Has Test [II - Nested object property denoted with a string]',
            () => {
                it(
                    'Should assert true when the object property does not exist.',
                    () => {
                        assert.equal(
                            _.has(
                                { deeply: { nested: { } } },
                                'deeply.nested.testProp'
                            ),
                            false
                        );
                    }
                );
            }
        );


        describe(
            'Get Test [I - Object property does not exist nor have a value]',
            () => {
                it(
                    'Should assert true, indicating the property did not exist and a default value returned.',
                    () => {
                        assert.equal(
                            _.get( {}, 'testProp', 'A default value.' ),
                            'A default value.'
                        );
                    }
                );
            }
        );


        describe(
            'Get Test [II - Object property does exist and has a value]',
            () => {
                it(
                    'Should assert true, indicating the property did exist and the expected value returned.',
                    () => {
                        assert.equal(
                            _.get( { testProp: "An existing value."}, 'testProp', 'A default value.' ),
                            'An existing value.'
                        );
                    }
                );
            }
        );


        describe(
            'Get Test [III - Nested object property does not exist nor have a value]',
            () => {
                it(
                    'Should assert true, indicating the nested property did not exist and a default value returned.',
                    () => {
                        assert.equal(
                            _.get( {}, 'deeply.nested.testProp', 'A default value.' ),
                            'A default value.'
                        );
                    }
                );
            }
        );


        describe(
            'Get Test [IV - Nested object property does exist and has a value]',
            () => {
                it(
                    'Should assert true, indicating the nested property did exist and the expected value returned.',
                    () => {
                        assert.equal(
                            _.get( { deeply: { nested: { testProp: "An existing value." } } }, 'deeply.nested.testProp', 'A default value.' ),
                            'An existing value.'
                        );
                    }
                );
            }
        );


        describe(
            'Set Test [I - Object property does not exist]',
            () => {
                it(
                    'Should assert true, indicating the property did not exist, was set, and its assigned value returned.',
                    () => {
                        assert.equal(
                            _.set( { testProp: "An existing value." }, 'newProp', 'A new property value.' ).newProp,
                            'A new property value.'
                        );
                    }
                );
            }
        );


        describe(
            'Set Test [II - Object property does exist and has value]',
            () => {
                it(
                    'Should assert true, indicating the property did exists, its value overwritten and new value returned.',
                    () => {
                        assert.equal(
                            _.set( { testProp: "An existing value." }, 'testProp', 'An updated value.' ).testProp,
                            'An updated value.'
                        );
                    }
                );
            }
        );


        describe(
            'Set Test [III - Nested object property does not exist]',
            () => {
                it(
                    'Should assert true, indicating the nested property did not exists, was set, and its assigned value returned.',
                    () => {
                        assert.equal(
                            _.set( { testProp: "An existing value." }, 'deeply.nested.newProp', 'A new property value.' ).newProp,
                            'A new property value.'
                        );
                    }
                );
            }
        );


        describe(
            'Set Test [IV - Nested object property does exist]',
            () => {
                it(
                    'Should assert true, indicating the nested property exists, its value overwritten and new value returned.',
                    () => {
                        assert.equal(
                            _.set( { deeply: { nested: { testProp: "An existing value." } } }, 'deeply.nested.testProp', 'An updated value.' ).testProp,
                            'An updated value.'
                        );
                    }
                );
            }
        );


        //https://www.typescriptlang.org/docs/handbook/mixins.html#alternative-pattern
        describe(
            'Compose Test [I - Extend class with multiple classes via mixin pattern]',
            () => {
                it(
                    'Should assert true, indicating a new class extended by all mixin classes was created.',
                    () => {
                        // Spec classes
                        class MiddleName {
                            middle: string = "B";
                            getMiddle(){ return this.middle; }
                        }
                        class LastName {
                            last: string = "Winters";
                            getLast(){ return this.last; }
                        }
                        class FirstName {
                            first: string = "Richard";
                            getFirst(){ return this.first; }
                        }
                        class Name {
                            constructor( first?: string, middle?: string, last?: string ) {
                                if( first )
                                    this.first = first;

                                if( middle )
                                    this.middle = middle;

                                if( last )
                                    this.last = last;
                            }

                            getName(){ return `${this.getFirst()} ${this.getMiddle()} ${this.getLast()}`; }
                        }

                        // Spec an interface of the derived class that extends other classes
                        interface Name extends FirstName, MiddleName, LastName {};

                        // Then apply extending classes to base class as mixins via the compose function
                        _.compose( Name, [ FirstName, MiddleName, LastName ] );

                        // Now create an instance of the derived, extended, class
                        const test = new Name( "Richard", "B.", "Winters" );

                        // And test the derived class inherited everything
                        assert.equal(
                            test.getName(),
                            'Richard B. Winters'
                        );
                    }
                );
            }
        );


    }
);
